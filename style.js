class Game {
  constructor(difficulty) {
    this.boardSize = 10;
    this.difficulty = difficulty;
    this.score = 0;
    this.timeInterval = this.getTimeInterval();
    this.gameOver = false;

    this.initializeBoard();
    this.startRound();
    this.redCount = 0;
    this.greenCount = 0;
    this.timerInterval = null; // Змінна для збереження інтервалу часу
  }

  getTimeInterval() {
    switch (this.difficulty) {
      case 'easy':
        return 1500;
      case 'medium':
        return 1000;
      case 'hard':
        return 500;
      default:
        return 1500;
    }
  }

  initializeBoard() {
    const gameBoard = document.getElementById('gameBoard');
    gameBoard.innerHTML = '';
    for (let i = 0; i < this.boardSize; i++) {
      const row = document.createElement('tr');
      for (let j = 0; j < this.boardSize; j++) {
        const cell = document.createElement('td');
        cell.onclick = () => this.handleClick(cell);
        row.appendChild(cell);
      }
      gameBoard.appendChild(row);
    }
  }

  startRound() {
    this.gameOver = false;
    this.score = 0;
    this.updateScoreDisplay();
    this.highlightRandomCell();
    this.timerInterval = setInterval(() => { // Встановлюємо інтервал
      this.endRound(); // Викликаємо endRound, якщо таймер досягнув кінця
    }, this.timeInterval * this.boardSize);
  }

  handleClick(cell) {
    if (!this.gameOver && cell.classList.contains('blue')) {
      this.score++;
      cell.classList.remove('blue');
      if (cell.classList.contains('green')) {
        cell.classList.add('green-clicked');
        this.greenCount++;
      } else {
        cell.classList.add('red-clicked');
        this.redCount++;
      }
      this.updateScoreDisplay();
      this.checkGameStatus();
    }
  }

  checkGameStatus() {
    const totalCells = this.boardSize * this.boardSize;
    const halfCells = Math.floor(totalCells / 2);
    if (this.redCount + this.greenCount >= halfCells) {
      this.endGame();
    }
  }

  highlightRandomCell() {
    const cells = document.getElementsByTagName('td');
    const randomCell = cells[Math.floor(Math.random() * cells.length)];
    randomCell.classList.add('blue');
    setTimeout(() => {
      randomCell.classList.remove('blue');
      if (!this.gameOver) {
        this.highlightRandomCell();
      }
    }, this.timeInterval);
  }

  endRound() {
    clearInterval(this.timerInterval); // Очищаємо інтервал
    this.gameOver = true;
    const cells = document.getElementsByTagName('td');
    for (let i = 0; i < cells.length; i++) {
      const cell = cells[i];
      if (cell.classList.contains('blue')) {
        cell.classList.add('missed');
        this.redCount++;
      }
    }
    if (this.greenCount > this.redCount) {
      alert('Congratulations! You win!');
    } else if (this.redCount > this.greenCount) {
      alert('You lose! Computer wins!');
    } else {
      alert('It\'s a tie!');
    }
  }

  updateScoreDisplay() {
    document.getElementById('score').innerText = `Score: ${this.score}`;
  }
}

function startGame(difficulty) {
  const game = new Game(difficulty);
}